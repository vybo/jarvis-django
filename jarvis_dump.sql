--
-- PostgreSQL database dump
--

-- Dumped from database version 9.5.1
-- Dumped by pg_dump version 9.5.1

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: jarvis; Type: DATABASE; Schema: -; Owner: dan
--

CREATE DATABASE jarvis WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'en_US.UTF-8' LC_CTYPE = 'en_US.UTF-8';


ALTER DATABASE jarvis OWNER TO dan;

\connect jarvis

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: auth_group; Type: TABLE; Schema: public; Owner: dan
--

CREATE TABLE auth_group (
    id integer NOT NULL,
    name character varying(80) NOT NULL
);


ALTER TABLE auth_group OWNER TO dan;

--
-- Name: auth_group_id_seq; Type: SEQUENCE; Schema: public; Owner: dan
--

CREATE SEQUENCE auth_group_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE auth_group_id_seq OWNER TO dan;

--
-- Name: auth_group_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dan
--

ALTER SEQUENCE auth_group_id_seq OWNED BY auth_group.id;


--
-- Name: auth_group_permissions; Type: TABLE; Schema: public; Owner: dan
--

CREATE TABLE auth_group_permissions (
    id integer NOT NULL,
    group_id integer NOT NULL,
    permission_id integer NOT NULL
);


ALTER TABLE auth_group_permissions OWNER TO dan;

--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE; Schema: public; Owner: dan
--

CREATE SEQUENCE auth_group_permissions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE auth_group_permissions_id_seq OWNER TO dan;

--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dan
--

ALTER SEQUENCE auth_group_permissions_id_seq OWNED BY auth_group_permissions.id;


--
-- Name: auth_permission; Type: TABLE; Schema: public; Owner: dan
--

CREATE TABLE auth_permission (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    content_type_id integer NOT NULL,
    codename character varying(100) NOT NULL
);


ALTER TABLE auth_permission OWNER TO dan;

--
-- Name: auth_permission_id_seq; Type: SEQUENCE; Schema: public; Owner: dan
--

CREATE SEQUENCE auth_permission_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE auth_permission_id_seq OWNER TO dan;

--
-- Name: auth_permission_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dan
--

ALTER SEQUENCE auth_permission_id_seq OWNED BY auth_permission.id;


--
-- Name: auth_user; Type: TABLE; Schema: public; Owner: dan
--

CREATE TABLE auth_user (
    id integer NOT NULL,
    password character varying(128) NOT NULL,
    last_login timestamp with time zone,
    is_superuser boolean NOT NULL,
    username character varying(30) NOT NULL,
    first_name character varying(30) NOT NULL,
    last_name character varying(30) NOT NULL,
    email character varying(254) NOT NULL,
    is_staff boolean NOT NULL,
    is_active boolean NOT NULL,
    date_joined timestamp with time zone NOT NULL
);


ALTER TABLE auth_user OWNER TO dan;

--
-- Name: auth_user_groups; Type: TABLE; Schema: public; Owner: dan
--

CREATE TABLE auth_user_groups (
    id integer NOT NULL,
    user_id integer NOT NULL,
    group_id integer NOT NULL
);


ALTER TABLE auth_user_groups OWNER TO dan;

--
-- Name: auth_user_groups_id_seq; Type: SEQUENCE; Schema: public; Owner: dan
--

CREATE SEQUENCE auth_user_groups_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE auth_user_groups_id_seq OWNER TO dan;

--
-- Name: auth_user_groups_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dan
--

ALTER SEQUENCE auth_user_groups_id_seq OWNED BY auth_user_groups.id;


--
-- Name: auth_user_id_seq; Type: SEQUENCE; Schema: public; Owner: dan
--

CREATE SEQUENCE auth_user_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE auth_user_id_seq OWNER TO dan;

--
-- Name: auth_user_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dan
--

ALTER SEQUENCE auth_user_id_seq OWNED BY auth_user.id;


--
-- Name: auth_user_user_permissions; Type: TABLE; Schema: public; Owner: dan
--

CREATE TABLE auth_user_user_permissions (
    id integer NOT NULL,
    user_id integer NOT NULL,
    permission_id integer NOT NULL
);


ALTER TABLE auth_user_user_permissions OWNER TO dan;

--
-- Name: auth_user_user_permissions_id_seq; Type: SEQUENCE; Schema: public; Owner: dan
--

CREATE SEQUENCE auth_user_user_permissions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE auth_user_user_permissions_id_seq OWNER TO dan;

--
-- Name: auth_user_user_permissions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dan
--

ALTER SEQUENCE auth_user_user_permissions_id_seq OWNED BY auth_user_user_permissions.id;


--
-- Name: django_admin_log; Type: TABLE; Schema: public; Owner: dan
--

CREATE TABLE django_admin_log (
    id integer NOT NULL,
    action_time timestamp with time zone NOT NULL,
    object_id text,
    object_repr character varying(200) NOT NULL,
    action_flag smallint NOT NULL,
    change_message text NOT NULL,
    content_type_id integer,
    user_id integer NOT NULL,
    CONSTRAINT django_admin_log_action_flag_check CHECK ((action_flag >= 0))
);


ALTER TABLE django_admin_log OWNER TO dan;

--
-- Name: django_admin_log_id_seq; Type: SEQUENCE; Schema: public; Owner: dan
--

CREATE SEQUENCE django_admin_log_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE django_admin_log_id_seq OWNER TO dan;

--
-- Name: django_admin_log_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dan
--

ALTER SEQUENCE django_admin_log_id_seq OWNED BY django_admin_log.id;


--
-- Name: django_content_type; Type: TABLE; Schema: public; Owner: dan
--

CREATE TABLE django_content_type (
    id integer NOT NULL,
    app_label character varying(100) NOT NULL,
    model character varying(100) NOT NULL
);


ALTER TABLE django_content_type OWNER TO dan;

--
-- Name: django_content_type_id_seq; Type: SEQUENCE; Schema: public; Owner: dan
--

CREATE SEQUENCE django_content_type_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE django_content_type_id_seq OWNER TO dan;

--
-- Name: django_content_type_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dan
--

ALTER SEQUENCE django_content_type_id_seq OWNED BY django_content_type.id;


--
-- Name: django_migrations; Type: TABLE; Schema: public; Owner: dan
--

CREATE TABLE django_migrations (
    id integer NOT NULL,
    app character varying(255) NOT NULL,
    name character varying(255) NOT NULL,
    applied timestamp with time zone NOT NULL
);


ALTER TABLE django_migrations OWNER TO dan;

--
-- Name: django_migrations_id_seq; Type: SEQUENCE; Schema: public; Owner: dan
--

CREATE SEQUENCE django_migrations_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE django_migrations_id_seq OWNER TO dan;

--
-- Name: django_migrations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dan
--

ALTER SEQUENCE django_migrations_id_seq OWNED BY django_migrations.id;


--
-- Name: django_session; Type: TABLE; Schema: public; Owner: dan
--

CREATE TABLE django_session (
    session_key character varying(40) NOT NULL,
    session_data text NOT NULL,
    expire_date timestamp with time zone NOT NULL
);


ALTER TABLE django_session OWNER TO dan;

--
-- Name: webui_device; Type: TABLE; Schema: public; Owner: dan
--

CREATE TABLE webui_device (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    user_id_id integer NOT NULL
);


ALTER TABLE webui_device OWNER TO dan;

--
-- Name: webui_device_id_seq; Type: SEQUENCE; Schema: public; Owner: dan
--

CREATE SEQUENCE webui_device_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE webui_device_id_seq OWNER TO dan;

--
-- Name: webui_device_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dan
--

ALTER SEQUENCE webui_device_id_seq OWNED BY webui_device.id;


--
-- Name: webui_entry; Type: TABLE; Schema: public; Owner: dan
--

CREATE TABLE webui_entry (
    id integer NOT NULL,
    date_time timestamp with time zone NOT NULL,
    entry_value character varying(255) NOT NULL,
    device_id_id integer NOT NULL,
    user_id_id integer NOT NULL,
    value_type_id_id integer NOT NULL
);


ALTER TABLE webui_entry OWNER TO dan;

--
-- Name: webui_entry_id_seq; Type: SEQUENCE; Schema: public; Owner: dan
--

CREATE SEQUENCE webui_entry_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE webui_entry_id_seq OWNER TO dan;

--
-- Name: webui_entry_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dan
--

ALTER SEQUENCE webui_entry_id_seq OWNED BY webui_entry.id;


--
-- Name: webui_output; Type: TABLE; Schema: public; Owner: dan
--

CREATE TABLE webui_output (
    id integer NOT NULL,
    output_text character varying(2000) NOT NULL,
    user_id_id integer NOT NULL
);


ALTER TABLE webui_output OWNER TO dan;

--
-- Name: webui_output_entry_id; Type: TABLE; Schema: public; Owner: dan
--

CREATE TABLE webui_output_entry_id (
    id integer NOT NULL,
    output_id integer NOT NULL,
    entry_id integer NOT NULL
);


ALTER TABLE webui_output_entry_id OWNER TO dan;

--
-- Name: webui_output_entry_id_id_seq; Type: SEQUENCE; Schema: public; Owner: dan
--

CREATE SEQUENCE webui_output_entry_id_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE webui_output_entry_id_id_seq OWNER TO dan;

--
-- Name: webui_output_entry_id_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dan
--

ALTER SEQUENCE webui_output_entry_id_id_seq OWNED BY webui_output_entry_id.id;


--
-- Name: webui_output_id_seq; Type: SEQUENCE; Schema: public; Owner: dan
--

CREATE SEQUENCE webui_output_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE webui_output_id_seq OWNER TO dan;

--
-- Name: webui_output_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dan
--

ALTER SEQUENCE webui_output_id_seq OWNED BY webui_output.id;


--
-- Name: webui_user; Type: TABLE; Schema: public; Owner: dan
--

CREATE TABLE webui_user (
    id integer NOT NULL,
    username character varying(100) NOT NULL,
    password character varying(255) NOT NULL,
    name character varying(100) NOT NULL,
    surname character varying(100) NOT NULL,
    picture character varying(500) NOT NULL
);


ALTER TABLE webui_user OWNER TO dan;

--
-- Name: webui_user_id_seq; Type: SEQUENCE; Schema: public; Owner: dan
--

CREATE SEQUENCE webui_user_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE webui_user_id_seq OWNER TO dan;

--
-- Name: webui_user_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dan
--

ALTER SEQUENCE webui_user_id_seq OWNED BY webui_user.id;


--
-- Name: webui_valuetype; Type: TABLE; Schema: public; Owner: dan
--

CREATE TABLE webui_valuetype (
    id integer NOT NULL,
    value_type character varying(50) NOT NULL
);


ALTER TABLE webui_valuetype OWNER TO dan;

--
-- Name: webui_valuetype_id_seq; Type: SEQUENCE; Schema: public; Owner: dan
--

CREATE SEQUENCE webui_valuetype_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE webui_valuetype_id_seq OWNER TO dan;

--
-- Name: webui_valuetype_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dan
--

ALTER SEQUENCE webui_valuetype_id_seq OWNED BY webui_valuetype.id;


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: dan
--

ALTER TABLE ONLY auth_group ALTER COLUMN id SET DEFAULT nextval('auth_group_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: dan
--

ALTER TABLE ONLY auth_group_permissions ALTER COLUMN id SET DEFAULT nextval('auth_group_permissions_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: dan
--

ALTER TABLE ONLY auth_permission ALTER COLUMN id SET DEFAULT nextval('auth_permission_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: dan
--

ALTER TABLE ONLY auth_user ALTER COLUMN id SET DEFAULT nextval('auth_user_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: dan
--

ALTER TABLE ONLY auth_user_groups ALTER COLUMN id SET DEFAULT nextval('auth_user_groups_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: dan
--

ALTER TABLE ONLY auth_user_user_permissions ALTER COLUMN id SET DEFAULT nextval('auth_user_user_permissions_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: dan
--

ALTER TABLE ONLY django_admin_log ALTER COLUMN id SET DEFAULT nextval('django_admin_log_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: dan
--

ALTER TABLE ONLY django_content_type ALTER COLUMN id SET DEFAULT nextval('django_content_type_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: dan
--

ALTER TABLE ONLY django_migrations ALTER COLUMN id SET DEFAULT nextval('django_migrations_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: dan
--

ALTER TABLE ONLY webui_device ALTER COLUMN id SET DEFAULT nextval('webui_device_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: dan
--

ALTER TABLE ONLY webui_entry ALTER COLUMN id SET DEFAULT nextval('webui_entry_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: dan
--

ALTER TABLE ONLY webui_output ALTER COLUMN id SET DEFAULT nextval('webui_output_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: dan
--

ALTER TABLE ONLY webui_output_entry_id ALTER COLUMN id SET DEFAULT nextval('webui_output_entry_id_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: dan
--

ALTER TABLE ONLY webui_user ALTER COLUMN id SET DEFAULT nextval('webui_user_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: dan
--

ALTER TABLE ONLY webui_valuetype ALTER COLUMN id SET DEFAULT nextval('webui_valuetype_id_seq'::regclass);


--
-- Data for Name: auth_group; Type: TABLE DATA; Schema: public; Owner: dan
--

COPY auth_group (id, name) FROM stdin;
\.


--
-- Name: auth_group_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dan
--

SELECT pg_catalog.setval('auth_group_id_seq', 1, false);


--
-- Data for Name: auth_group_permissions; Type: TABLE DATA; Schema: public; Owner: dan
--

COPY auth_group_permissions (id, group_id, permission_id) FROM stdin;
\.


--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dan
--

SELECT pg_catalog.setval('auth_group_permissions_id_seq', 1, false);


--
-- Data for Name: auth_permission; Type: TABLE DATA; Schema: public; Owner: dan
--

COPY auth_permission (id, name, content_type_id, codename) FROM stdin;
1	Can add log entry	1	add_logentry
2	Can change log entry	1	change_logentry
3	Can delete log entry	1	delete_logentry
4	Can add permission	2	add_permission
5	Can change permission	2	change_permission
6	Can delete permission	2	delete_permission
7	Can add group	3	add_group
8	Can change group	3	change_group
9	Can delete group	3	delete_group
10	Can add user	4	add_user
11	Can change user	4	change_user
12	Can delete user	4	delete_user
13	Can add content type	5	add_contenttype
14	Can change content type	5	change_contenttype
15	Can delete content type	5	delete_contenttype
16	Can add session	6	add_session
17	Can change session	6	change_session
18	Can delete session	6	delete_session
19	Can add user	7	add_user
20	Can change user	7	change_user
21	Can delete user	7	delete_user
22	Can add device	8	add_device
23	Can change device	8	change_device
24	Can delete device	8	delete_device
25	Can add value type	9	add_valuetype
26	Can change value type	9	change_valuetype
27	Can delete value type	9	delete_valuetype
28	Can add entry	10	add_entry
29	Can change entry	10	change_entry
30	Can delete entry	10	delete_entry
31	Can add output	11	add_output
32	Can change output	11	change_output
33	Can delete output	11	delete_output
\.


--
-- Name: auth_permission_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dan
--

SELECT pg_catalog.setval('auth_permission_id_seq', 33, true);


--
-- Data for Name: auth_user; Type: TABLE DATA; Schema: public; Owner: dan
--

COPY auth_user (id, password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined) FROM stdin;
1	pbkdf2_sha256$20000$fyc609AoJfNL$2qPN2Le6deJ2TUOB/zDCpk0FLRM+BOTI0gNQG96ua64=	2016-02-24 13:29:58.940652+01	t	dan			xvybira5@mendelu.cz	t	t	2016-02-22 16:40:24.294388+01
\.


--
-- Data for Name: auth_user_groups; Type: TABLE DATA; Schema: public; Owner: dan
--

COPY auth_user_groups (id, user_id, group_id) FROM stdin;
\.


--
-- Name: auth_user_groups_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dan
--

SELECT pg_catalog.setval('auth_user_groups_id_seq', 1, false);


--
-- Name: auth_user_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dan
--

SELECT pg_catalog.setval('auth_user_id_seq', 1, true);


--
-- Data for Name: auth_user_user_permissions; Type: TABLE DATA; Schema: public; Owner: dan
--

COPY auth_user_user_permissions (id, user_id, permission_id) FROM stdin;
\.


--
-- Name: auth_user_user_permissions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dan
--

SELECT pg_catalog.setval('auth_user_user_permissions_id_seq', 1, false);


--
-- Data for Name: django_admin_log; Type: TABLE DATA; Schema: public; Owner: dan
--

COPY django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) FROM stdin;
1	2016-02-22 16:43:05.477857+01	1	testing	1		7	1
2	2016-02-22 16:43:39.900893+01	1	Testing device	1		8	1
3	2016-02-22 16:43:56.3525+01	1	testing value	1		9	1
4	2016-02-22 16:49:56.896603+01	3	testing value testing value	1		10	1
5	2016-02-22 16:50:05.03283+01	3	testing value testing value	2	No fields changed.	10	1
6	2016-02-24 13:30:10.942422+01	1	testing value type	2	Changed value_type.	9	1
\.


--
-- Name: django_admin_log_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dan
--

SELECT pg_catalog.setval('django_admin_log_id_seq', 6, true);


--
-- Data for Name: django_content_type; Type: TABLE DATA; Schema: public; Owner: dan
--

COPY django_content_type (id, app_label, model) FROM stdin;
1	admin	logentry
2	auth	permission
3	auth	group
4	auth	user
5	contenttypes	contenttype
6	sessions	session
7	webui	user
8	webui	device
9	webui	valuetype
10	webui	entry
11	webui	output
\.


--
-- Name: django_content_type_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dan
--

SELECT pg_catalog.setval('django_content_type_id_seq', 11, true);


--
-- Data for Name: django_migrations; Type: TABLE DATA; Schema: public; Owner: dan
--

COPY django_migrations (id, app, name, applied) FROM stdin;
1	contenttypes	0001_initial	2016-02-22 14:39:32.925508+01
2	auth	0001_initial	2016-02-22 14:39:32.987792+01
3	admin	0001_initial	2016-02-22 14:39:33.010048+01
4	contenttypes	0002_remove_content_type_name	2016-02-22 14:39:33.044516+01
5	auth	0002_alter_permission_name_max_length	2016-02-22 14:39:33.056603+01
6	auth	0003_alter_user_email_max_length	2016-02-22 14:39:33.068711+01
7	auth	0004_alter_user_username_opts	2016-02-22 14:39:33.07919+01
8	auth	0005_alter_user_last_login_null	2016-02-22 14:39:33.090975+01
9	auth	0006_require_contenttypes_0002	2016-02-22 14:39:33.093761+01
10	sessions	0001_initial	2016-02-22 14:39:33.1045+01
11	webui	0001_initial	2016-02-22 16:33:15.077337+01
12	webui	0002_auto_20160222_1536	2016-02-22 16:36:39.600953+01
13	webui	0003_auto_20160222_1624	2016-02-22 17:24:28.63538+01
\.


--
-- Name: django_migrations_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dan
--

SELECT pg_catalog.setval('django_migrations_id_seq', 13, true);


--
-- Data for Name: django_session; Type: TABLE DATA; Schema: public; Owner: dan
--

COPY django_session (session_key, session_data, expire_date) FROM stdin;
x9wxpf7t0sj9zadq9aoyyh9oxj2tby08	NGI3NjAyOGQ0YTJiYmM5Mzk2NzE2YmY0YjE0ZWYzZDg2YTJmYzhkNDp7Il9hdXRoX3VzZXJfaGFzaCI6IjAwZmVjOTZjYWU3ZDc1NTgxOWNlZDRjNmRhZjA5ZDExMjEyM2YwYWUiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiIxIn0=	2016-03-09 13:29:58.942403+01
\.


--
-- Data for Name: webui_device; Type: TABLE DATA; Schema: public; Owner: dan
--

COPY webui_device (id, name, user_id_id) FROM stdin;
1	Testing device	1
\.


--
-- Name: webui_device_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dan
--

SELECT pg_catalog.setval('webui_device_id_seq', 1, true);


--
-- Data for Name: webui_entry; Type: TABLE DATA; Schema: public; Owner: dan
--

COPY webui_entry (id, date_time, entry_value, device_id_id, user_id_id, value_type_id_id) FROM stdin;
3	2016-02-22 16:44:06+01	testing value	1	1	1
\.


--
-- Name: webui_entry_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dan
--

SELECT pg_catalog.setval('webui_entry_id_seq', 3, true);


--
-- Data for Name: webui_output; Type: TABLE DATA; Schema: public; Owner: dan
--

COPY webui_output (id, output_text, user_id_id) FROM stdin;
\.


--
-- Data for Name: webui_output_entry_id; Type: TABLE DATA; Schema: public; Owner: dan
--

COPY webui_output_entry_id (id, output_id, entry_id) FROM stdin;
\.


--
-- Name: webui_output_entry_id_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dan
--

SELECT pg_catalog.setval('webui_output_entry_id_id_seq', 1, false);


--
-- Name: webui_output_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dan
--

SELECT pg_catalog.setval('webui_output_id_seq', 1, false);


--
-- Data for Name: webui_user; Type: TABLE DATA; Schema: public; Owner: dan
--

COPY webui_user (id, username, password, name, surname, picture) FROM stdin;
1	testing	testingpassword	Testing	User	none
\.


--
-- Name: webui_user_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dan
--

SELECT pg_catalog.setval('webui_user_id_seq', 1, true);


--
-- Data for Name: webui_valuetype; Type: TABLE DATA; Schema: public; Owner: dan
--

COPY webui_valuetype (id, value_type) FROM stdin;
1	testing value type
\.


--
-- Name: webui_valuetype_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dan
--

SELECT pg_catalog.setval('webui_valuetype_id_seq', 1, true);


--
-- Name: auth_group_name_key; Type: CONSTRAINT; Schema: public; Owner: dan
--

ALTER TABLE ONLY auth_group
    ADD CONSTRAINT auth_group_name_key UNIQUE (name);


--
-- Name: auth_group_permissions_group_id_permission_id_key; Type: CONSTRAINT; Schema: public; Owner: dan
--

ALTER TABLE ONLY auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_group_id_permission_id_key UNIQUE (group_id, permission_id);


--
-- Name: auth_group_permissions_pkey; Type: CONSTRAINT; Schema: public; Owner: dan
--

ALTER TABLE ONLY auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_pkey PRIMARY KEY (id);


--
-- Name: auth_group_pkey; Type: CONSTRAINT; Schema: public; Owner: dan
--

ALTER TABLE ONLY auth_group
    ADD CONSTRAINT auth_group_pkey PRIMARY KEY (id);


--
-- Name: auth_permission_content_type_id_codename_key; Type: CONSTRAINT; Schema: public; Owner: dan
--

ALTER TABLE ONLY auth_permission
    ADD CONSTRAINT auth_permission_content_type_id_codename_key UNIQUE (content_type_id, codename);


--
-- Name: auth_permission_pkey; Type: CONSTRAINT; Schema: public; Owner: dan
--

ALTER TABLE ONLY auth_permission
    ADD CONSTRAINT auth_permission_pkey PRIMARY KEY (id);


--
-- Name: auth_user_groups_pkey; Type: CONSTRAINT; Schema: public; Owner: dan
--

ALTER TABLE ONLY auth_user_groups
    ADD CONSTRAINT auth_user_groups_pkey PRIMARY KEY (id);


--
-- Name: auth_user_groups_user_id_group_id_key; Type: CONSTRAINT; Schema: public; Owner: dan
--

ALTER TABLE ONLY auth_user_groups
    ADD CONSTRAINT auth_user_groups_user_id_group_id_key UNIQUE (user_id, group_id);


--
-- Name: auth_user_pkey; Type: CONSTRAINT; Schema: public; Owner: dan
--

ALTER TABLE ONLY auth_user
    ADD CONSTRAINT auth_user_pkey PRIMARY KEY (id);


--
-- Name: auth_user_user_permissions_pkey; Type: CONSTRAINT; Schema: public; Owner: dan
--

ALTER TABLE ONLY auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_pkey PRIMARY KEY (id);


--
-- Name: auth_user_user_permissions_user_id_permission_id_key; Type: CONSTRAINT; Schema: public; Owner: dan
--

ALTER TABLE ONLY auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_user_id_permission_id_key UNIQUE (user_id, permission_id);


--
-- Name: auth_user_username_key; Type: CONSTRAINT; Schema: public; Owner: dan
--

ALTER TABLE ONLY auth_user
    ADD CONSTRAINT auth_user_username_key UNIQUE (username);


--
-- Name: django_admin_log_pkey; Type: CONSTRAINT; Schema: public; Owner: dan
--

ALTER TABLE ONLY django_admin_log
    ADD CONSTRAINT django_admin_log_pkey PRIMARY KEY (id);


--
-- Name: django_content_type_app_label_45f3b1d93ec8c61c_uniq; Type: CONSTRAINT; Schema: public; Owner: dan
--

ALTER TABLE ONLY django_content_type
    ADD CONSTRAINT django_content_type_app_label_45f3b1d93ec8c61c_uniq UNIQUE (app_label, model);


--
-- Name: django_content_type_pkey; Type: CONSTRAINT; Schema: public; Owner: dan
--

ALTER TABLE ONLY django_content_type
    ADD CONSTRAINT django_content_type_pkey PRIMARY KEY (id);


--
-- Name: django_migrations_pkey; Type: CONSTRAINT; Schema: public; Owner: dan
--

ALTER TABLE ONLY django_migrations
    ADD CONSTRAINT django_migrations_pkey PRIMARY KEY (id);


--
-- Name: django_session_pkey; Type: CONSTRAINT; Schema: public; Owner: dan
--

ALTER TABLE ONLY django_session
    ADD CONSTRAINT django_session_pkey PRIMARY KEY (session_key);


--
-- Name: webui_device_pkey; Type: CONSTRAINT; Schema: public; Owner: dan
--

ALTER TABLE ONLY webui_device
    ADD CONSTRAINT webui_device_pkey PRIMARY KEY (id);


--
-- Name: webui_entry_pkey; Type: CONSTRAINT; Schema: public; Owner: dan
--

ALTER TABLE ONLY webui_entry
    ADD CONSTRAINT webui_entry_pkey PRIMARY KEY (id);


--
-- Name: webui_output_entry_id_output_id_entry_id_key; Type: CONSTRAINT; Schema: public; Owner: dan
--

ALTER TABLE ONLY webui_output_entry_id
    ADD CONSTRAINT webui_output_entry_id_output_id_entry_id_key UNIQUE (output_id, entry_id);


--
-- Name: webui_output_entry_id_pkey; Type: CONSTRAINT; Schema: public; Owner: dan
--

ALTER TABLE ONLY webui_output_entry_id
    ADD CONSTRAINT webui_output_entry_id_pkey PRIMARY KEY (id);


--
-- Name: webui_output_pkey; Type: CONSTRAINT; Schema: public; Owner: dan
--

ALTER TABLE ONLY webui_output
    ADD CONSTRAINT webui_output_pkey PRIMARY KEY (id);


--
-- Name: webui_user_pkey; Type: CONSTRAINT; Schema: public; Owner: dan
--

ALTER TABLE ONLY webui_user
    ADD CONSTRAINT webui_user_pkey PRIMARY KEY (id);


--
-- Name: webui_valuetype_pkey; Type: CONSTRAINT; Schema: public; Owner: dan
--

ALTER TABLE ONLY webui_valuetype
    ADD CONSTRAINT webui_valuetype_pkey PRIMARY KEY (id);


--
-- Name: auth_group_name_253ae2a6331666e8_like; Type: INDEX; Schema: public; Owner: dan
--

CREATE INDEX auth_group_name_253ae2a6331666e8_like ON auth_group USING btree (name varchar_pattern_ops);


--
-- Name: auth_group_permissions_0e939a4f; Type: INDEX; Schema: public; Owner: dan
--

CREATE INDEX auth_group_permissions_0e939a4f ON auth_group_permissions USING btree (group_id);


--
-- Name: auth_group_permissions_8373b171; Type: INDEX; Schema: public; Owner: dan
--

CREATE INDEX auth_group_permissions_8373b171 ON auth_group_permissions USING btree (permission_id);


--
-- Name: auth_permission_417f1b1c; Type: INDEX; Schema: public; Owner: dan
--

CREATE INDEX auth_permission_417f1b1c ON auth_permission USING btree (content_type_id);


--
-- Name: auth_user_groups_0e939a4f; Type: INDEX; Schema: public; Owner: dan
--

CREATE INDEX auth_user_groups_0e939a4f ON auth_user_groups USING btree (group_id);


--
-- Name: auth_user_groups_e8701ad4; Type: INDEX; Schema: public; Owner: dan
--

CREATE INDEX auth_user_groups_e8701ad4 ON auth_user_groups USING btree (user_id);


--
-- Name: auth_user_user_permissions_8373b171; Type: INDEX; Schema: public; Owner: dan
--

CREATE INDEX auth_user_user_permissions_8373b171 ON auth_user_user_permissions USING btree (permission_id);


--
-- Name: auth_user_user_permissions_e8701ad4; Type: INDEX; Schema: public; Owner: dan
--

CREATE INDEX auth_user_user_permissions_e8701ad4 ON auth_user_user_permissions USING btree (user_id);


--
-- Name: auth_user_username_51b3b110094b8aae_like; Type: INDEX; Schema: public; Owner: dan
--

CREATE INDEX auth_user_username_51b3b110094b8aae_like ON auth_user USING btree (username varchar_pattern_ops);


--
-- Name: django_admin_log_417f1b1c; Type: INDEX; Schema: public; Owner: dan
--

CREATE INDEX django_admin_log_417f1b1c ON django_admin_log USING btree (content_type_id);


--
-- Name: django_admin_log_e8701ad4; Type: INDEX; Schema: public; Owner: dan
--

CREATE INDEX django_admin_log_e8701ad4 ON django_admin_log USING btree (user_id);


--
-- Name: django_session_de54fa62; Type: INDEX; Schema: public; Owner: dan
--

CREATE INDEX django_session_de54fa62 ON django_session USING btree (expire_date);


--
-- Name: django_session_session_key_461cfeaa630ca218_like; Type: INDEX; Schema: public; Owner: dan
--

CREATE INDEX django_session_session_key_461cfeaa630ca218_like ON django_session USING btree (session_key varchar_pattern_ops);


--
-- Name: webui_device_18624dd3; Type: INDEX; Schema: public; Owner: dan
--

CREATE INDEX webui_device_18624dd3 ON webui_device USING btree (user_id_id);


--
-- Name: webui_entry_18624dd3; Type: INDEX; Schema: public; Owner: dan
--

CREATE INDEX webui_entry_18624dd3 ON webui_entry USING btree (user_id_id);


--
-- Name: webui_entry_99de7f0f; Type: INDEX; Schema: public; Owner: dan
--

CREATE INDEX webui_entry_99de7f0f ON webui_entry USING btree (value_type_id_id);


--
-- Name: webui_entry_b81be047; Type: INDEX; Schema: public; Owner: dan
--

CREATE INDEX webui_entry_b81be047 ON webui_entry USING btree (device_id_id);


--
-- Name: webui_output_18624dd3; Type: INDEX; Schema: public; Owner: dan
--

CREATE INDEX webui_output_18624dd3 ON webui_output USING btree (user_id_id);


--
-- Name: webui_output_entry_id_b64a62ea; Type: INDEX; Schema: public; Owner: dan
--

CREATE INDEX webui_output_entry_id_b64a62ea ON webui_output_entry_id USING btree (entry_id);


--
-- Name: webui_output_entry_id_f7f1d83a; Type: INDEX; Schema: public; Owner: dan
--

CREATE INDEX webui_output_entry_id_f7f1d83a ON webui_output_entry_id USING btree (output_id);


--
-- Name: auth_content_type_id_508cf46651277a81_fk_django_content_type_id; Type: FK CONSTRAINT; Schema: public; Owner: dan
--

ALTER TABLE ONLY auth_permission
    ADD CONSTRAINT auth_content_type_id_508cf46651277a81_fk_django_content_type_id FOREIGN KEY (content_type_id) REFERENCES django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_group_permissio_group_id_689710a9a73b7457_fk_auth_group_id; Type: FK CONSTRAINT; Schema: public; Owner: dan
--

ALTER TABLE ONLY auth_group_permissions
    ADD CONSTRAINT auth_group_permissio_group_id_689710a9a73b7457_fk_auth_group_id FOREIGN KEY (group_id) REFERENCES auth_group(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_group_permission_id_1f49ccbbdc69d2fc_fk_auth_permission_id; Type: FK CONSTRAINT; Schema: public; Owner: dan
--

ALTER TABLE ONLY auth_group_permissions
    ADD CONSTRAINT auth_group_permission_id_1f49ccbbdc69d2fc_fk_auth_permission_id FOREIGN KEY (permission_id) REFERENCES auth_permission(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user__permission_id_384b62483d7071f0_fk_auth_permission_id; Type: FK CONSTRAINT; Schema: public; Owner: dan
--

ALTER TABLE ONLY auth_user_user_permissions
    ADD CONSTRAINT auth_user__permission_id_384b62483d7071f0_fk_auth_permission_id FOREIGN KEY (permission_id) REFERENCES auth_permission(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_groups_group_id_33ac548dcf5f8e37_fk_auth_group_id; Type: FK CONSTRAINT; Schema: public; Owner: dan
--

ALTER TABLE ONLY auth_user_groups
    ADD CONSTRAINT auth_user_groups_group_id_33ac548dcf5f8e37_fk_auth_group_id FOREIGN KEY (group_id) REFERENCES auth_group(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_groups_user_id_4b5ed4ffdb8fd9b0_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: dan
--

ALTER TABLE ONLY auth_user_groups
    ADD CONSTRAINT auth_user_groups_user_id_4b5ed4ffdb8fd9b0_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_user_permiss_user_id_7f0938558328534a_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: dan
--

ALTER TABLE ONLY auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permiss_user_id_7f0938558328534a_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: djan_content_type_id_697914295151027a_fk_django_content_type_id; Type: FK CONSTRAINT; Schema: public; Owner: dan
--

ALTER TABLE ONLY django_admin_log
    ADD CONSTRAINT djan_content_type_id_697914295151027a_fk_django_content_type_id FOREIGN KEY (content_type_id) REFERENCES django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: django_admin_log_user_id_52fdd58701c5f563_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: dan
--

ALTER TABLE ONLY django_admin_log
    ADD CONSTRAINT django_admin_log_user_id_52fdd58701c5f563_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: webui_device_user_id_id_62c4ebbf589d2693_fk_webui_user_id; Type: FK CONSTRAINT; Schema: public; Owner: dan
--

ALTER TABLE ONLY webui_device
    ADD CONSTRAINT webui_device_user_id_id_62c4ebbf589d2693_fk_webui_user_id FOREIGN KEY (user_id_id) REFERENCES webui_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: webui_e_value_type_id_id_67228a7e6aea62db_fk_webui_valuetype_id; Type: FK CONSTRAINT; Schema: public; Owner: dan
--

ALTER TABLE ONLY webui_entry
    ADD CONSTRAINT webui_e_value_type_id_id_67228a7e6aea62db_fk_webui_valuetype_id FOREIGN KEY (value_type_id_id) REFERENCES webui_valuetype(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: webui_entry_device_id_id_3f5e8bccc8c75d07_fk_webui_device_id; Type: FK CONSTRAINT; Schema: public; Owner: dan
--

ALTER TABLE ONLY webui_entry
    ADD CONSTRAINT webui_entry_device_id_id_3f5e8bccc8c75d07_fk_webui_device_id FOREIGN KEY (device_id_id) REFERENCES webui_device(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: webui_entry_user_id_id_703b1b52e0491438_fk_webui_user_id; Type: FK CONSTRAINT; Schema: public; Owner: dan
--

ALTER TABLE ONLY webui_entry
    ADD CONSTRAINT webui_entry_user_id_id_703b1b52e0491438_fk_webui_user_id FOREIGN KEY (user_id_id) REFERENCES webui_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: webui_output_entr_output_id_514913b79d02f5c1_fk_webui_output_id; Type: FK CONSTRAINT; Schema: public; Owner: dan
--

ALTER TABLE ONLY webui_output_entry_id
    ADD CONSTRAINT webui_output_entr_output_id_514913b79d02f5c1_fk_webui_output_id FOREIGN KEY (output_id) REFERENCES webui_output(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: webui_output_entry__entry_id_5a96826680796543_fk_webui_entry_id; Type: FK CONSTRAINT; Schema: public; Owner: dan
--

ALTER TABLE ONLY webui_output_entry_id
    ADD CONSTRAINT webui_output_entry__entry_id_5a96826680796543_fk_webui_entry_id FOREIGN KEY (entry_id) REFERENCES webui_entry(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: webui_output_user_id_id_52f0a57f31a75f62_fk_webui_user_id; Type: FK CONSTRAINT; Schema: public; Owner: dan
--

ALTER TABLE ONLY webui_output
    ADD CONSTRAINT webui_output_user_id_id_52f0a57f31a75f62_fk_webui_user_id FOREIGN KEY (user_id_id) REFERENCES webui_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

