from django.contrib import admin
from django.contrib.gis import admin

# Register your models here.
from .models import User
from .models import Output
from .models import Device
from .models import Entry
from .models import ValueType
from .models import VisitClassification
from .models import Visit
from .models import TimeCountStore
from .models import LengthsStore
from .models import PlaceClassification
from .models import Place


admin.site.register(User)
admin.site.register(Output)
admin.site.register(Device)
admin.site.register(Entry)
admin.site.register(ValueType)
admin.site.register(VisitClassification)
admin.site.register(Visit, admin.OSMGeoAdmin)
admin.site.register(TimeCountStore)
admin.site.register(LengthsStore)
admin.site.register(PlaceClassification)
admin.site.register(Place)
