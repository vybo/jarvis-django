from datetime import datetime
from .models import User
from .models import LengthsStore
from .models import TimeCountStore
from .models import PlaceClassification
from .models import Place
import core


def run(timelinefile, userid):
    #places = core.get_places_web('/Users/dan/jarvis-compute-git/jsons/LocationHistory.json')
    places = core.get_places_web(timelinefile)

    start_time = datetime.now()
    print ("[[Django]]===========[PLACES DATABASE PROCESSING]===========")
    print("[load_places]Got places. Storing to DB. Started at " + start_time.strftime('%Y-%m-%d %H:%M:%S'))

    user = User.objects.get(pk=userid)

    for place in places:
        latitude = place['latitude']
        longitude = place['longitude']
        name = place['name']
        place_type = PlaceClassification.objects.get(description=place['type'])
        count = place['count']
        home_probability = place['home_probability']
        work_probability = place['work_probability']

        dbplace = Place(latitude=latitude, longitude=longitude, name=name, place_type=place_type, count=count,
                      home_probability=home_probability, work_probability=work_probability, user=user)
        dbplace.save()

        for hour in range(0,len(place['starting_time_count'])):
            time_count_store_start = TimeCountStore(value1=hour, value2=place['starting_time_count'][hour], belongs_to=dbplace, is_ending_time=False)
            time_count_store_start.save()
            time_count_store_end = TimeCountStore(value1=hour, value2=place['ending_time_count'][hour], belongs_to=dbplace, is_ending_time=True)
            time_count_store_end.save()

        for length in place['lengths']:
            length_store = LengthsStore(value=length, belongs_to=dbplace)
            length_store.save()

        print("[load_places] Stored place")

    end_time = datetime.now()
    difference_time_seconds = end_time - start_time
    print("[load_places ] Places stored in DB. Finished in " + str(difference_time_seconds) + ".")
