import os
from django.contrib.gis.utils import LayerMapping
from datetime import datetime
from .models import Visit
from .models import User
from .models import VisitClassification
import core

'''
if set(['entry_datetime', 'value', 'value_type', 'user', 'device']).issubset(request.POST.keys()):
        datetime = request.POST['entry_datetime']
        value = request.POST['value']
        value_type = request.POST['value_type']
        user = request.POST['user']
        device = request.POST['device']

        entry = models.Entry(date_time=datetime, entry_value=value, user_id=user, value_type_id=value_type,
                             device_id=device)

        entry.save()
        index()

'''


def run(timelinefile, userid):
    #visits = core.get_visits_web('/Users/dan/jarvis-compute-git/jsons/LocationHistory.json')
    visits = core.get_visits_web(timelinefile)

    start_time = datetime.now()
    total = visits['meta']['count']
    processed = 0

    print ("[[Django]]===========[VISITS DATABASE PROCESSING]===========")
    print("[load_visits]Got visits. Storing to DB. Started at " + start_time.strftime('%Y-%m-%d %H:%M:%S'))

    user = User.objects.get(pk=userid)

    for year in visits.keys():
        if year == 'meta':
            pass
        else:
            for month in visits[year].keys():
                for day in visits[year][month].keys():
                    for visit in visits[year][month][day]['visits']:
                        starting_time = datetime.fromtimestamp(visit['starting_time_timestampS'])
                        ending_time = datetime.fromtimestamp(visit['ending_time_timestampS'])
                        latitude = visit['latitude']
                        longitude = visit['longitude']
                        length = visit['ending_time_timestampS'] - visit['starting_time_timestampS']

                        if 'classification' in visit.keys():
                            if 'travel' == visit['classification']:
                                classification = VisitClassification.objects.get(description='travel')
                            if 'visit' == visit['classification']:
                                classification = VisitClassification.objects.get(description='visit')

                        else:
                            classification = VisitClassification.objects.get(description='none')

                        visit = Visit(starting_time=starting_time, ending_time=ending_time, latitude=latitude,
                                      longitude=longitude, length=length, user=user,
                                      visit_classification=classification)

                        visit.save()
                        processed += 1

                print("[load_visits]Progress: " + str((processed/total) * 100)) + " %."

    end_time = datetime.now()
    difference_time_seconds = end_time - start_time
    print("[load_visits]Entries stored in DB. Finished in " + str(difference_time_seconds) + ".")
