# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Device',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(default=b'unknown', max_length=255)),
            ],
        ),
        migrations.CreateModel(
            name='Entry',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('date_time', models.DateTimeField(verbose_name=b'entry time')),
                ('value', models.CharField(max_length=255)),
                ('device_id', models.ForeignKey(to='webui.Device')),
            ],
        ),
        migrations.CreateModel(
            name='Output',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('output_text', models.CharField(max_length=2000)),
                ('entry_id', models.ManyToManyField(to='webui.Entry')),
            ],
        ),
        migrations.CreateModel(
            name='User',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('username', models.CharField(max_length=100)),
                ('password', models.CharField(max_length=255)),
                ('name', models.CharField(max_length=100)),
                ('surname', models.CharField(max_length=100)),
                ('picture', models.CharField(max_length=500)),
            ],
        ),
        migrations.CreateModel(
            name='ValueType',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('value_type', models.CharField(max_length=50)),
            ],
        ),
        migrations.AddField(
            model_name='output',
            name='user_id',
            field=models.ForeignKey(to='webui.User'),
        ),
        migrations.AddField(
            model_name='entry',
            name='user_id',
            field=models.ForeignKey(to='webui.User'),
        ),
        migrations.AddField(
            model_name='entry',
            name='value_type_id',
            field=models.ForeignKey(to='webui.ValueType'),
        ),
        migrations.AddField(
            model_name='device',
            name='user_id',
            field=models.ForeignKey(to='webui.User'),
        ),
    ]
