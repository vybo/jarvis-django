# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('webui', '0005_auto_20160318_1330'),
    ]

    operations = [
        migrations.AddField(
            model_name='visit',
            name='classification',
            field=models.CharField(max_length=150, null=True),
        ),
        migrations.AddField(
            model_name='visit',
            name='length',
            field=models.FloatField(default=0),
            preserve_default=False,
        ),
    ]
