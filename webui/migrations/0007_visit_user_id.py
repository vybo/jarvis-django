# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('webui', '0006_auto_20160321_1038'),
    ]

    operations = [
        migrations.AddField(
            model_name='visit',
            name='user_id',
            field=models.ForeignKey(default=1, to='webui.User'),
            preserve_default=False,
        ),
    ]
