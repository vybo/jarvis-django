# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('webui', '0007_visit_user_id'),
    ]

    operations = [
        migrations.RenameField(
            model_name='device',
            old_name='user_id',
            new_name='user',
        ),
        migrations.RenameField(
            model_name='entry',
            old_name='device_id',
            new_name='device',
        ),
        migrations.RenameField(
            model_name='entry',
            old_name='user_id',
            new_name='user',
        ),
        migrations.RenameField(
            model_name='entry',
            old_name='value_type_id',
            new_name='value_type',
        ),
        migrations.RenameField(
            model_name='output',
            old_name='entry_id',
            new_name='entry',
        ),
        migrations.RenameField(
            model_name='output',
            old_name='user_id',
            new_name='user',
        ),
        migrations.RenameField(
            model_name='visit',
            old_name='user_id',
            new_name='user',
        ),
    ]
