# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('webui', '0008_auto_20160321_1507'),
    ]

    operations = [
        migrations.CreateModel(
            name='VisitClassification',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('description', models.CharField(max_length=150)),
            ],
        ),
        migrations.RemoveField(
            model_name='visit',
            name='classification',
        ),
        migrations.AddField(
            model_name='visit',
            name='visit_classification',
            field=models.ForeignKey(to='webui.VisitClassification', null=True),
        ),
    ]
