# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('webui', '0009_auto_20160323_1316'),
    ]

    operations = [
        migrations.CreateModel(
            name='FloatStore',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('value', models.FloatField()),
            ],
        ),
        migrations.CreateModel(
            name='IntegerPair',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('value1', models.IntegerField()),
                ('value2', models.IntegerField()),
            ],
        ),
        migrations.CreateModel(
            name='IntegerStore',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('value', models.IntegerField()),
            ],
        ),
        migrations.CreateModel(
            name='Place',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('latitude', models.FloatField()),
                ('longitude', models.FloatField()),
                ('name', models.CharField(max_length=300)),
                ('count', models.IntegerField()),
                ('home_probability', models.FloatField()),
                ('work_probability', models.FloatField()),
            ],
        ),
        migrations.CreateModel(
            name='PlaceClassification',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('description', models.CharField(max_length=150)),
            ],
        ),
        migrations.AddField(
            model_name='place',
            name='type',
            field=models.ForeignKey(to='webui.PlaceClassification'),
        ),
        migrations.AddField(
            model_name='integerstore',
            name='belongs_to',
            field=models.ForeignKey(to='webui.Place'),
        ),
        migrations.AddField(
            model_name='integerpair',
            name='belongs_to',
            field=models.ForeignKey(to='webui.Place'),
        ),
        migrations.AddField(
            model_name='floatstore',
            name='belongs_to',
            field=models.ForeignKey(to='webui.Place'),
        ),
    ]
