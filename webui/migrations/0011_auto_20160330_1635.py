# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('webui', '0010_auto_20160330_1611'),
    ]

    operations = [
        migrations.CreateModel(
            name='TimeCountStore',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('value1', models.IntegerField()),
                ('value2', models.IntegerField()),
                ('is_ending_time', models.BooleanField()),
                ('belongs_to', models.ForeignKey(to='webui.Place')),
            ],
        ),
        migrations.RenameModel(
            old_name='FloatStore',
            new_name='LengthsStore',
        ),
        migrations.RemoveField(
            model_name='integerpair',
            name='belongs_to',
        ),
        migrations.RemoveField(
            model_name='integerstore',
            name='belongs_to',
        ),
        migrations.DeleteModel(
            name='IntegerPair',
        ),
        migrations.DeleteModel(
            name='IntegerStore',
        ),
    ]
