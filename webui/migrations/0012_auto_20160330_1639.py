# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('webui', '0011_auto_20160330_1635'),
    ]

    operations = [
        migrations.RenameField(
            model_name='place',
            old_name='type',
            new_name='place_type',
        ),
    ]
