from __future__ import unicode_literals

from django.db import models
from django.utils.encoding import python_2_unicode_compatible
from django.contrib.gis.db import models


# Create your models here.
@python_2_unicode_compatible
class User(models.Model):
    username = models.CharField(max_length=100)
    password = models.CharField(max_length=255)
    name = models.CharField(max_length=100)
    surname = models.CharField(max_length=100)
    picture = models.CharField(max_length=500)

    def __str__(self):
        return self.username + ", id: " + str(self.id)


@python_2_unicode_compatible
class Device(models.Model):
    name = models.CharField(max_length=255, default='unknown')
    user = models.ForeignKey(User, on_delete=models.CASCADE)

    def __str__(self):
        return self.name


@python_2_unicode_compatible
class ValueType(models.Model):
    value_type = models.CharField(max_length=50)

    def __str__(self):
        return self.value_type


@python_2_unicode_compatible
class Entry(models.Model):
    date_time = models.DateTimeField('entry_time')
    entry_value = models.CharField(max_length=255)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    value_type = models.ForeignKey(ValueType)
    device = models.ForeignKey(Device)

    def __str__(self):
        return str(self.user) + ", " + str(self.value_type) + " " + self.entry_value


@python_2_unicode_compatible
class Output(models.Model):
    output_text = models.CharField(max_length=2000)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    entry = models.ManyToManyField(Entry)

    def __str__(self):
        return str(self.user) + "::" + self.output_text


@python_2_unicode_compatible
class VisitClassification(models.Model):
    description = models.CharField(max_length=150)

    def __str__(self):
        return str(self.description)


@python_2_unicode_compatible
class Visit(models.Model):
    starting_time = models.DateTimeField()
    ending_time = models.DateTimeField()
    longitude = models.FloatField()
    latitude = models.FloatField()
    length = models.FloatField()
    visit_classification = models.ForeignKey(VisitClassification, null=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE)

    # Returns the string representation of the model.
    def __str__(self):              # __unicode__ on Python 2
        string_start = self.starting_time.strftime('%Y-%m-%d %H:%M:%S')
        string_end = self.ending_time.strftime('%Y-%m-%d %H:%M:%S')
        classification = VisitClassification.objects.get(id=self.visit_classification_id)
        string = "Visit from " + string_start + " to " + string_end + " of type: " + classification.description
        return string


@python_2_unicode_compatible
class PlaceClassification(models.Model):
    description = models.CharField(max_length=150)

    def __str__(self):
        return str(self.description)


@python_2_unicode_compatible
class Place(models.Model):
    latitude = models.FloatField()
    longitude = models.FloatField()
    name = models.CharField(max_length=300)
    place_type = models.ForeignKey(PlaceClassification)
    count = models.IntegerField()
    home_probability = models.FloatField()
    work_probability = models.FloatField()
    user = models.ForeignKey(User, on_delete=models.CASCADE)

    def __str__(self):
        return unicode(self.name + ", " + self.place_type.description + ", " + str(self.count))


# Used for storing arrays of floats
@python_2_unicode_compatible
class LengthsStore(models.Model):
    value = models.FloatField()
    belongs_to = models.ForeignKey(Place, on_delete=models.CASCADE)

    def __str__(self):
        return str(self.value) + ", place: " + self.belongs_to.name + " " + str(self.belongs_to.count)


# Used for storing arrays of integer pairs
@python_2_unicode_compatible
class TimeCountStore(models.Model):
    value1 = models.IntegerField()
    value2 = models.IntegerField()
    is_ending_time = models.BooleanField()
    belongs_to = models.ForeignKey(Place, on_delete=models.CASCADE)

    def __str__(self):
        return str(self.value1) + " : " + str(self.value2) + ", place: " + self.belongs_to.name + " " + str(self.belongs_to.count)
