from django.conf.urls import url

from . import views
from django.views.decorators.csrf import csrf_exempt


app_name = 'webui'
urlpatterns = [
    # ex: /webui/
    url(r'^$', views.index, name='index'),
    # ex: /webui/entries/1/
    url(r'^entry/(?P<entry_id>[0-9]+)/$', views.entry_detail, name='entry_detail'),
    url(r'^entry_new/$', views.entry_new, name='entry_new'),
    url(r'^entry_new_post/$', csrf_exempt(views.entry_new_post), name='entry_new_post'),
    url(r'^process_visits/$', views.process_visits, name='process_visits'),
    url(r'^process_places/$', views.process_places, name='process_places'),
    url(r'^places/$', views.places, name='places'),
    url(r'^data_processing/$', views.processing, name='processing'),
    url(r'^handle_processing/$', views.handle_processing, name='handle_processing'),
]
