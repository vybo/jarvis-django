from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse
from django.http import Http404
from django.http import HttpResponseBadRequest
from . import models
import load_visits
import load_places
from datetime import datetime
import datetime
from multiprocessing import Process


# Create your views here.
def index(request):
    classifications = models.VisitClassification.objects.all()
    users = models.User.objects.all()

    if set(['date_from', 'date_to', 'classification_id', 'user_id']).issubset(request.POST.keys()):

        startingtime = datetime.strptime(request.POST['date_from'], '%d.%m.%Y %H:%M')
        endingtime = datetime.strptime(request.POST['date_to'], '%d.%m.%Y %H:%M')

        if request.POST['classification_id'] == '0' and request.POST['user_id'] == '0':
            visits = models.Visit.objects.filter(starting_time__range=(startingtime, endingtime),
                                                 ending_time__range=(startingtime, endingtime),
                                                 )
        elif request.POST['user_id'] == '0':
            classif = models.VisitClassification.objects.get(id=int(request.POST['classification_id']))
            visits = models.Visit.objects.filter(starting_time__range=(startingtime, endingtime),
                                                 ending_time__range=(startingtime, endingtime),
                                                 visit_classification=classif)
        elif request.POST['classification_id'] == '0':
            user = models.User.objects.get(id=int(request.POST['user_id']))
            visits = models.Visit.objects.filter(starting_time__range=(startingtime, endingtime),
                                                 ending_time__range=(startingtime, endingtime),
                                                 user=user)
        else:
            classif = models.VisitClassification.objects.get(id=int(request.POST['classification_id']))
            user = models.User.objects.get(id=int(request.POST['user_id']))
            visits = models.Visit.objects.filter(starting_time__range=(startingtime, endingtime),
                                                 ending_time__range=(startingtime, endingtime),
                                                 visit_classification=classif, user=user)

        #visits = models.Visit.objects.all()
        context = {
            'active': 'index',
            'classifications_list': classifications,
            'users_list': users,
            'date_from': startingtime.strftime('%m/%d/%Y %H:%M'),
            'date_to': endingtime.strftime('%m/%d/%Y %H:%M'),
            'user_id': int(request.POST['user_id']),
            'classification_id': int(request.POST['classification_id']),
            'visits_list': visits
        }
    else:
        startingtime = datetime.strptime('7.5.2015 00:00', '%d.%m.%Y %H:%M')
        endingtime = datetime.strptime('7.5.2015 23:59', '%d.%m.%Y %H:%M')

        visits = models.Visit.objects.filter(starting_time__range=(startingtime, endingtime),
                                                 ending_time__range=(startingtime, endingtime),
                                                 )
        context = {
            'active': 'index',
            'classifications_list': classifications,
            'users_list': users,
            'date_from': startingtime.strftime('%m/%d/%Y %H:%M'),
            'date_to': endingtime.strftime('%m/%d/%Y %H:%M'),
            'user_id': None,
            'classification_id': None,
            'visits_list': visits
        }

    return render(request, 'webui/index.html', context)


def entry_detail(request, entry_id):
    entry = get_object_or_404(models.Entry, pk=entry_id)

    return render(request, 'webui/entry_detail.html', {'entry': entry})


def user_detail(request, user_id):
    response = "You're looking at entry %s."
    return HttpResponse(response % user_id)


def output_detail(request, output_id):
    return HttpResponse("You're looking at entry %s." % output_id)


def device_detail(request, device_id):
    return HttpResponse("You're looking at entry %s." % device_id)


def entry_new(request):
    users = models.User.objects.all()
    value_types = models.ValueType.objects.all()
    devices = models.Device.objects.all()
    context = {
        'users' : users,
        'value_types' : value_types,
        'devices' : devices,
    }

    return render(request, 'webui/entry_new.html', context)


def strip_optional(s):
    return s[s.find("(")+1:s.find(")")]


# Method for adding entry from a mobile device
def entry_new_post(request):
    if set(['entry_datetime', 'value', 'value_type', 'user', 'device']).issubset(request.POST.keys()):

        datetime_timestamp = strip_optional(request.POST['entry_datetime']).split('.')
        datetime_datetime = datetime.datetime.fromtimestamp(int(datetime_timestamp[0]))
        # datetime.strptime(request.POST['date_to'], '%d.%m.%Y %H:%M')
        value = strip_optional(request.POST['value'])
        value_type = strip_optional(request.POST['value_type'])
        value_type = models.ValueType.objects.get(value_type=value_type)

        user = int(request.POST['user'])
        device = int(request.POST['device'])

        user = models.User.objects.get(id=user)
        device = models.Device.objects.get(id=device)

        entry = models.Entry(date_time=datetime_datetime, entry_value=value, user=user, value_type=value_type,
                             device=device)

        entry.save()
        return HttpResponse(status=201) # Entry created

    else:
        users = models.User.objects.all()
        value_types = models.ValueType.objects.all()
        devices = models.Device.objects.all()
        context = {
            'users' : users,
            'value_types' : value_types,
            'devices' : devices,
            'error_message': "All values must be entered.",
        }

        return HttpResponseBadRequest('Incorrect use of the application\'s API.')


def process_visits(request):
    load_visits.run()
    context = {
        'message':'Processed data.'
    }
    return render(request, 'webui/process_visits.html', context)


def process_places(request):
    load_places.run()
    context = {
        'message':'Processed data.'
    }
    return render(request, 'webui/process_visits.html', context)


def places(request):
    place_types = models.PlaceClassification.objects.all()
    users = models.User.objects.all()

    if set(['place_type_id', 'candidate_for', 'times_visited', 'user_id']).issubset(request.POST.keys()):
        if request.POST['place_type_id'] == '0' and request.POST['user_id'] == '0':
            resplaces = models.Place.objects.filter(count__gte=int(request.POST['times_visited']))
        elif request.POST['user_id'] == '0':
            place_type = models.PlaceClassification.objects.get(id=int(request.POST['place_type_id']))
            resplaces = models.Place.objects.filter(place_type=place_type, count__gte=int(request.POST['times_visited']))
        elif request.POST['place_type_id'] == '0':
            user = models.User.objects.get(id=int(request.POST['user_id']))
            resplaces = models.Place.objects.filter(user=user, count__gte=int(request.POST['times_visited']))
        else:
            place_type = models.PlaceClassification.objects.get(id=int(request.POST['place_type_id']))
            user = models.User.objects.get(id=int(request.POST['user_id']))
            resplaces = models.Place.objects.filter(place_type=place_type, user=user, count__gte=int(request.POST['times_visited']))

        context = {
            'active': 'places',
            'place_types_list': place_types,
            'users_list': users,
            'user_id': int(request.POST['user_id']),
            'place_type_id': int(request.POST['place_type_id']),
            'times_visited': int(request.POST['times_visited']),
            'candidate_for': request.POST['candidate_for'],
            'places_list': resplaces
        }
    else:
        resplaces = models.Visit.objects.all()
        context = {
            'active': 'places',
            'place_types_list': place_types,
            'users_list': users,
            'user_id': None,
            'place_type_id': None,
            'times_visited': 0,
            'places_list': resplaces
        }

    return render(request, 'webui/places.html', context)


def processing(request):
    users = models.User.objects.all()

    context = {
            'active': 'processing',
            'users_list': users,
    }
    return render(request, 'webui/processing.html', context)


def handle_processing(request):
    if set(['user_id']).issubset(request.POST.keys()) and len(request.FILES) > 0:
        timeline_file = request.FILES['timelinefile']
        userid = request.POST['user_id']

        if 'process_visits' in request.POST:
            process_visits(timeline_file,userid)

        if 'process_places' in request.POST:
            process_places(timeline_file,userid)

    context = {
            'active': 'none',
            'message_title': 'File is being processed',
            'message_text': 'File upload is complete. The file will be processed in background. When the processing '
                            'is complete, you will see the outcome in other parts of this application.'
    }
    return render(request, 'webui/message.html', context)


# TODO These need to run asyncly
def process_visits(timelinefile, userid):
    # TODO this temporary file path is shit
    visits_process = Process(target=load_visits.run(timelinefile.temporary_file_path(), userid))
    visits_process.start()


def process_places(timelinefile, userid):
    # TODO this temporary file path is shit
    places_process = Process(target=load_places.run(timelinefile.temporary_file_path(), userid))
    places_process.start()

